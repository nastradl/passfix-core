﻿using System.ComponentModel;

namespace PassFix.Enums
{
    public enum EnumAuditEvents
    {
        [Description("User logged in successfully")]
        LoginSuccess = 1,
        [Description("User logged out")]
        LogOut,
        [Description("User requested to be registered")]
        RequestAdded,
        [Description("User request approved and new user created")]
        RequestApproved,
        [Description("User request denied")]
        RequestDenied,
        [Description("Add claim to user")]
        ClaimAdded,
        [Description("Remove claim to user")]
        ClaimRemoved,
        [Description("User disabled")]
        DisableUser,
        [Description("User updated personal details")]
        PersonalDetailsUpdated,
        [Description("User changed password")]
        PasswordChanged,
        [Description("Search enrolprofile table")]
        SearchEnrolProfile,
        [Description("Update enrolprofile stagecode from EM4000 to EM2000 and set enrollocationid to 9115001")]
        UpdateBackToApproval,
        [Description("Update enrolprofile stagecode from EM0801 to EM0800")]
        UpdateBackToPaymentCheck,
        [Description("Update persoprofile and docprofile stagecode from PM2000 to PM2001")]
        UpdateBackToPerso,
        [Description("Update enrolprofile stagecode from EM2001 to EM1500")]
        UpdateAfisRecord,
        [Description("Update record details")]
        UpdateRecordDetails

    }
}
