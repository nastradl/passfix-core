﻿using System.Globalization;
using System.Text.RegularExpressions;

namespace PassFix.Extensions
{
    public static class StringExtensions
    {
        public static string ToTitleCase(this string s)
        {
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(s.ToLower());
        }

        public static string TitleCaseToWords(this string s)
        {
            return Regex.Replace(s, "(\\B[A-Z])", " $1");
        }
    }
}
