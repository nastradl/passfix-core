﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using PassFix.Data;
using PassFix.Enums;
using PassFix.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using PassFix.Models.Central;

namespace PassFix.Extensions
{
    public class DbContextExtensions
    {
        private static AuthDbContext _context;
        private static UserManager<ApplicationUser> _userManager;
        private static string _adminPw;

        public DbContextExtensions(AuthDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public void Seed(IServiceProvider serviceProvider, string adminPassword)
        {
            _adminPw = adminPassword;

            // Perform database delete and create
            //_context.Database.EnsureDeleted();
            _context.Database.EnsureCreated();

            // Perform seed operations
            AddAuditEvents(_context);

            AddDefaultAdmin(_context);

            AddClaims(_context);

            //Run scripts if you need to repopulate the database with lists and such
            SeedData(_context);

            // Save changes and release resources
            _context.SaveChanges();
            _context.Dispose();
        }
        
        private void AddDefaultAdmin(AuthDbContext context)
        {
            var user = new ApplicationUser
            {
                FirstName = "Default",
                Surname = "Administrator",
                Email = "admin@irissmart.com",
                NormalizedEmail = "ADMIN@IRISSMART.COM",
                UserName = "Administrator",
                NormalizedUserName = "ADMINISTRATOR",
                EmailConfirmed = true,
                DateCreated = DateTime.Now,
                PhoneNumberConfirmed = true,
                SecurityStamp = Guid.NewGuid().ToString("D")
            };
            
            if (!context.Users.Any(u => u.UserName == user.UserName))
            {
                var password = new PasswordHasher<ApplicationUser>();
                var hashed = password.HashPassword(user, _adminPw);
                user.PasswordHash = hashed;

                var userStore = new UserStore<ApplicationUser>(context);
                userStore.CreateAsync(user).Wait();

                // Add user administrator claims
                _userManager.AddClaimsAsync(user, new List<Claim>
                {
                    new Claim("UserManagement", "ManageMyAccount"),
                    new Claim("UserManagement", "ManageOtherUsers"),
                    new Claim("UserManagement", "ManageUserRequests"),
                    new Claim("AMU", "EditEnrolmentRecord"),
                    new Claim("AMU", "BackToApproval"),
                    new Claim("AMU", "BackToPerso"),
                    new Claim("AMU", "IssuePassport"),
                    new Claim("AMU", "QueryBookletInfo"),
                    new Claim("AMU", "OverideBookletSequence"),
                    new Claim("AMU", "BackToAfisCheck"),
                    new Claim("AMU", "BackToPaymentCheck"),

                }).Wait();

                context.SaveChanges();
            }
        }
        
        private static void AddAuditEvents(AuthDbContext context)
        {
            // Convert enums to table data
            context.ListAuditEvents.SeedEnumValues<AuditEvent, EnumAuditEvents>(@enum => @enum);
        }

        private static void AddClaims(AuthDbContext context)
        {
            // Add default claims
            var claims = new List<UserClaim>
            {
                new UserClaim
                {
                    Controller = "UserManagement",
                    Action = "ManageMyAccount"
                },
                new UserClaim
                {
                    Controller = "UserManagement",
                    Action = "ManageOtherUsers"
                },
                new UserClaim
                {
                    Controller = "UserManagement",
                    Action = "ManageUserRequests"
                },
                new UserClaim
                {
                    Controller = "AMU",
                    Action = "EditEnrolmentRecord"
                },
                new UserClaim
                {
                    Controller = "AMU",
                    Action = "BackToApproval"
                },
                new UserClaim
                {
                    Controller = "AMU",
                    Action = "BackToPerso"
                },
                new UserClaim
                {
                    Controller = "AMU",
                    Action = "IssuePassport"
                },
                new UserClaim
                {
                    Controller = "AMU",
                    Action = "QueryBookletInfo"
                },
                new UserClaim
                {
                    Controller = "AMU",
                    Action = "OverideBookletSequence"
                },
                new UserClaim
                {
                    Controller = "AMU",
                    Action = "BackToAfisCheck"
                },
                new UserClaim
                {
                    Controller = "AMU",
                    Action = "BackToPaymentCheck"
                }
            };

            // Add any claims not already in the database
            var missingClaims = claims.Where(x => !context.ListUserClaims.Any(z => z.Controller == x.Controller && z.Action == x.Action)).ToList();
            context.ListUserClaims.AddRange(missingClaims);
        }

        public void SeedData(AuthDbContext context)
        {
            // NOTE: Please make sure the tables which depend on others (FK) are put in last
            var info = Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).Parent?.Parent?.Parent;
            if (info != null)
            {
                var filePaths = Directory.GetFiles(Path.Combine(info.FullName, "Data", "SeedScripts"), "*.sql", SearchOption.TopDirectoryOnly);

                foreach (var path in filePaths)
                {
                    context.Database.ExecuteSqlCommand(File.ReadAllText(path));
                }
            }
        }
    }
}
