﻿using System.ComponentModel.DataAnnotations;

namespace PassFix.Models.Central
{
    public class UserClaim
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Controller { get; set; }

        [Required]
        [StringLength(50)]
        public string Action { get; set; }
    }
}
