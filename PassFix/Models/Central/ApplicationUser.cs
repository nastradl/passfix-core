﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;
using PassFix.Extensions;

namespace PassFix.Models.Central
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        [Required]
        [StringLength(20)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(20)]
        public string Surname { get; set; }

        [StringLength(1)]
        public string Gender { get; set; }

        [Required]
        public DateTime DateCreated { get; set; } = DateTime.Now;

        [DefaultValue(1)]
        public bool IsFirstTimeLogin { get; set; }

        [NotMapped]
        public string NewPassword { get; set; }

        [NotMapped]
        public string CurrentPassword { get; set; }

        [NotMapped]
        public string NewPasswordConfirm { get; set; }

        public string GetFullName()
        {
            return FirstName.ToTitleCase() + " " + Surname.ToTitleCase();
        }
    }
}
