﻿using PassFix.Models.Central;

namespace PassFix.Models.AccountViewModels
{
    public class RegistrationModel
    {
        public LoginViewModel LoginViewModel { get; set; }

        public ForgotPasswordViewModel ForgotPasswordViewModel { get; set; }

        public UserRequest Request { get; set; }
    }
}
