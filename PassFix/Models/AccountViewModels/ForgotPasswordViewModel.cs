﻿using System.ComponentModel.DataAnnotations;

namespace PassFix.Models.AccountViewModels
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
