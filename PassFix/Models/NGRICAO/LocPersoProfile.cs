﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PassFix.Models.NGRICAO
{
    [Table("Loc_PersoProfile")]
    public class LocPersoProfile
    {
        [Key]
        [StringLength(12)]
        public string Formno { get; set; }

        [StringLength(12)]
        public string Docno { get; set; }

        public DateTime? Persotime { get; set; }

        [Required]
        [StringLength(5)]
        public string Branchcode { get; set; }

        [Required]
        [StringLength(6)]
        public string Stagecode { get; set; }

        public long? Persolocationid { get; set; }

        public long? Layoutid { get; set; }

        [Required]
        [StringLength(2)]
        public string Doctype { get; set; }

        public DateTime? Issuedate { get; set; }

        public DateTime? Expirydate { get; set; }

        public int? Priority { get; set; }

        [StringLength(50)]
        public string Remarks { get; set; }

        [StringLength(18)]
        public string Chipsn { get; set; }
    }
}
