﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PassFix.Models.NGRICAO
{
    [Table("Loc_Control")]
    public class LocControl
    {
        [Key]
        [StringLength(3)]
        public string ControlValue { get; set; }

        [StringLength(20)]
        public string ControlName { get; set; }

        [StringLength(60)]
        public string ControlDescription { get; set; }
    }
}
