using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using PassFix.Models;
using PassFix.Models.Central;
using PassFix.Models.NGRICAO;

namespace PassFix.Data
{
    public class NgricaoDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<LocEnrolProfile> LocEnrolProfile { get; set; }

        public DbSet<LocDocProfile> LocDocProfile { get; set; }

        public DbSet<LocDocHolderBioProfile> LocDocHolderBioProfile { get; set; }

        public DbSet<LocDocHolderCustomProfile> LocDocHolderCustomProfile { get; set; }

        public DbSet<LocBranch> LocBranch { get; set; }

        public DbSet<LocPaymentHistory> LocPaymentHistory { get; set; }

        public DbSet<LocPersoProfile> LocPersoProfile { get; set; }

        public DbSet<LocDocHolderMainProfile> LocDocHolderMainProfile { get; set; }




        public NgricaoDbContext(DbContextOptions<NgricaoDbContext> options) : base(options)
        {
           
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }
    }
}
