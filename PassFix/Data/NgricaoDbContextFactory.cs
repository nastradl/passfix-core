﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace PassFix.Data
{
    public class NgricaoDbContextFactory : IDesignTimeDbContextFactory<NgricaoDbContext>
    {
        public NgricaoDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<NgricaoDbContext>();
            var configuration = new ConfigurationBuilder()
                .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                .AddJsonFile("appsettings.json")
                .Build();

            builder.UseSqlServer(args.Length != 0 ? args[0] : configuration.GetConnectionString("NGRICAO"));
            return new NgricaoDbContext(builder.Options);
        }


    }
}
