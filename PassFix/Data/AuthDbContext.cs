﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using PassFix.Models.Central;

namespace PassFix.Data
{
    public class AuthDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<AuditEvent> ListAuditEvents { get; set; }
        public DbSet<UserClaim> ListUserClaims { get; set; }
        public DbSet<State> ListStates { get; set; }
        public DbSet<Title> ListTitles { get; set; }


        public DbSet<Audit> Audits { get; set; }
        public DbSet<ErrorLog> ErrorLogs { get; set; }
        public DbSet<UserRequest> UserRequests { get; set; }

        public AuthDbContext(DbContextOptions<AuthDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<ApplicationUser>()
                .Property(p => p.IsFirstTimeLogin)
                .HasDefaultValue(1);
        }
    }
}
