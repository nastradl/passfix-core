IF (EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_NAME = 'ListStates'))
				 BEGIN
DELETE FROM [ListStates]
DBCC CHECKIDENT ('ListStates', RESEED, 0)
END
ELSE
CREATE TABLE [ListStates](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL
 CONSTRAINT [PK_ListStates] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF
SET IDENTITY_INSERT [ListStates] ON 

INSERT[ListStates] ([id], [name]) VALUES (1, N'Abia')
INSERT[ListStates] ([id], [name]) VALUES (2, N'Adamawa')
INSERT[ListStates] ([id], [name]) VALUES (3, N'Akwa Ibom')
INSERT[ListStates] ([id], [name]) VALUES (4, N'Anambra')
INSERT[ListStates] ([id], [name]) VALUES (5, N'Bauchi')
INSERT[ListStates] ([id], [name]) VALUES (6, N'Bayelsa')
INSERT[ListStates] ([id], [name]) VALUES (7, N'Benue')
INSERT[ListStates] ([id], [name]) VALUES (8, N'Borno')
INSERT[ListStates] ([id], [name]) VALUES (9, N'Cross River')
INSERT[ListStates] ([id], [name]) VALUES (10, N'Delta')
INSERT[ListStates] ([id], [name]) VALUES (11, N'Ebonyi')
INSERT[ListStates] ([id], [name]) VALUES (12, N'Edo')
INSERT[ListStates] ([id], [name]) VALUES (13, N'Ekiti')
INSERT[ListStates] ([id], [name]) VALUES (14, N'Enugu')
INSERT[ListStates] ([id], [name]) VALUES (15, N'FCT')
INSERT[ListStates] ([id], [name]) VALUES (16, N'Gombe')
INSERT[ListStates] ([id], [name]) VALUES (17, N'Imo')
INSERT[ListStates] ([id], [name]) VALUES (18, N'Jigawa')
INSERT[ListStates] ([id], [name]) VALUES (19, N'Kaduna')
INSERT[ListStates] ([id], [name]) VALUES (20, N'Kano')
INSERT[ListStates] ([id], [name]) VALUES (21, N'Katsina')
INSERT[ListStates] ([id], [name]) VALUES (22, N'Kebbi')
INSERT[ListStates] ([id], [name]) VALUES (23, N'Kogi')
INSERT[ListStates] ([id], [name]) VALUES (24, N'Kwara')
INSERT[ListStates] ([id], [name]) VALUES (25, N'Lagos')
INSERT[ListStates] ([id], [name]) VALUES (26, N'Nasarawa')
INSERT[ListStates] ([id], [name]) VALUES (27, N'Niger')
INSERT[ListStates] ([id], [name]) VALUES (28, N'Ogun')
INSERT[ListStates] ([id], [name]) VALUES (29, N'Ondo')
INSERT[ListStates] ([id], [name]) VALUES (30, N'Osun')
INSERT[ListStates] ([id], [name]) VALUES (31, N'Oyo')
INSERT[ListStates] ([id], [name]) VALUES (32, N'Plateau')
INSERT[ListStates] ([id], [name]) VALUES (33, N'Rivers')
INSERT[ListStates] ([id], [name]) VALUES (34, N'Sokoto')
INSERT[ListStates] ([id], [name]) VALUES (35, N'Taraba')
INSERT[ListStates] ([id], [name]) VALUES (36, N'Yobe')
INSERT[ListStates] ([id], [name]) VALUES (37, N'Zamfara')
SET IDENTITY_INSERT[ListStates] OFF

