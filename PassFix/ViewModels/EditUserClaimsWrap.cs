﻿using System.Collections.Generic;

namespace PassFix.ViewModels
{
    public class EditUserClaimsWrap
    {
        public string UserId { get; set; }

        public List<EditUserClaims> Claims { get; set; }
    }
}
