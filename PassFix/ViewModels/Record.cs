﻿namespace PassFix.ViewModels
{
    public class Record
    {
        public string FormNo { get; set; }

        public string DocNo { get; set; }

        public string FirstName { get; set; }

        public string Surname { get; set; }

        public string MiddleName { get; set; }

        public long LocationId { get; set; }

        public string BranchCode { get; set; }

        public string StageCode { get; set; }

        public string PassportNo { get; set; }

        public string PassportType { get; set; }

        public string Error { get; set; }

        public string Title { get; set; }

        public string UpdateUrl { get; set; }

        public string SearchUrl { get; set; }

        public bool ShowPassportNo { get; set; }

        public bool ShowPassportType { get; set; }

        public bool DisableStagecode { get; set; }

        public bool ShowSearchByDocNo { get; set; }

        public bool AllowEditLocation { get; set; } = false;

        public string UpdateMsg { get; set; } = "Update";
    }
}
