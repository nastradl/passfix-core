﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using PassFix.Data;
using PassFix.Enums;
using PassFix.Extensions;
using PassFix.Models.Central;
using PassFix.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PassFix.Services
{
    public class AuthService : IAuthRepository
    {
        private readonly UserManager<ApplicationUser> _userManager;

        private readonly AuthDbContext _context;

        public AuthService(AuthDbContext ctx, UserManager<ApplicationUser> userManager)
        {
            _context = ctx;
            _userManager = userManager;
        }


        public async Task<int> GetNumberOfUsers()
        {
            return await _context.Users.CountAsync();
        }

        public async Task<List<State>> GetStates()
        {
            return await _context.ListStates.ToListAsync();
        }

        public async Task<List<Title>> GetTitles()
        {
            return await _context.ListTitles.ToListAsync();
        }

        public async Task<List<ApplicationUser>> GetUsers()
        {
            return await _context.Users.ToListAsync();
        }

        public async Task<List<ApplicationUser>> GetDisabledUsers()
        {
            return await _context.Users.Where(x => x.LockoutEnabled).ToListAsync();
        }

        public async Task<List<UserClaim>> GetClaimsList()
        {
            return await _context.ListUserClaims.ToListAsync();
        }

        public async Task<int> AddClaimsToUser(string userId, string claims, string operatorId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            var claimslist = new List<Claim>();
            foreach (string claim in claims.Split(','))
            {
                var type = claim.Split('/')[0].Trim();
                var value = claim.Split('/')[1].Trim();

                claimslist.Add(new Claim(type, value));
            }

            await _userManager.AddClaimsAsync(user, claimslist);
            
            // add audit
            _context.Audits.Add(new Audit
            {
                EventId = (int)EnumAuditEvents.ClaimAdded,
                Description = EnumAuditEvents.ClaimAdded.GetEnumDescription(),
                TimeStamp = DateTime.Now,
                UserId = operatorId
            });

            return await _context.SaveChangesAsync();
        }

        public async Task<int> UpdateUserDetails(ApplicationUser user)
        {
            var appuser = await _context.Users.SingleAsync(x => x.Id == user.Id);

            // if its a different username, confirm it is unique
            if (!string.Equals(appuser.UserName, user.UserName, StringComparison.CurrentCultureIgnoreCase))
            {
                var exists = await _userManager.FindByNameAsync(user.UserName.ToLower());

                if (exists != null)
                    return -1;
            }

            appuser.UserName = user.UserName;
            appuser.FirstName = user.FirstName;
            appuser.Surname = user.Surname;
            appuser.PhoneNumber = user.PhoneNumber;
            appuser.Gender = user.Gender;

            // add audit
            _context.Audits.Add(new Audit
            {
                EventId = (int)EnumAuditEvents.PersonalDetailsUpdated,
                Description = EnumAuditEvents.ClaimAdded.GetEnumDescription(),
                TimeStamp = DateTime.Now,
                UserId = user.Id
            });

            await _context.SaveChangesAsync();

            return 1;
        }

        public async Task<int> UpdateUserPassword(ApplicationUser user)
        {
            var appuser = await _context.Users.SingleAsync(x => x.Id == user.Id);
            var result = await _userManager.ChangePasswordAsync(appuser, user.CurrentPassword, user.NewPassword);

            // add audit
            _context.Audits.Add(new Audit
            {
                EventId = (int)EnumAuditEvents.PasswordChanged,
                Description = EnumAuditEvents.ClaimAdded.GetEnumDescription(),
                TimeStamp = DateTime.Now,
                UserId = user.Id
            });

            await _context.SaveChangesAsync();

            if (result.Succeeded)
                return 1;
            
            return 0;
        }

        public async Task<int> RemoveClaimsFromUser(string userId, string claims, string operatorId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            var claimslist = new List<Claim>();
            foreach (string claim in claims.Split(','))
            {
                var type = claim.Split('/')[0].Trim();
                var value = claim.Split('/')[1].Trim();

                claimslist.Add(new Claim(type, value));
            }

            await _userManager.RemoveClaimsAsync(user, claimslist);

            // add audit
            _context.Audits.Add(new Audit
            {
                EventId = (int)EnumAuditEvents.ClaimAdded,
                Description = EnumAuditEvents.ClaimAdded.GetEnumDescription(),
                TimeStamp = DateTime.Now,
                UserId = operatorId
            });

            return await _context.SaveChangesAsync();
        }

        public async Task<int> SetUserLockout(string userId, string operatorId, bool lockout)
        {
            var user = await _userManager.FindByIdAsync(userId);
            user.LockoutEnabled = lockout;

            // add audit
            _context.Audits.Add(new Audit
            {
                EventId = (int)EnumAuditEvents.DisableUser,
                Description = EnumAuditEvents.DisableUser.GetEnumDescription(),
                TimeStamp = DateTime.Now,
                UserId = operatorId
            });

            await _context.SaveChangesAsync();

            return 1;
        }

        public async Task<UserRequest> GetUserRequest(int id)
        {
            return await _context.UserRequests.SingleAsync(x => x.Id == id);
        }

        public async Task<List<UserRequest>> GetUserRequests()
        {
            return await _context.UserRequests.Where(x => x.Approved == null && x.IsActive).ToListAsync();
        }

        public async Task<bool> CheckEmailRegistered(string email)
        {
            // Check if there is a user with this email
            bool exists = await _context.Users.AnyAsync(x => x.Email.ToLower() == email.ToLower());

            if (exists)
                return true;
            
            // Check if there is a request with this email
            return await _context.UserRequests.AnyAsync(x => x.EmailAddress.ToLower() == email.ToLower());
        }

        public async Task<int> AddUserRequest(UserRequest request)
        {
            _context.UserRequests.Add(request);

            // Add audit
            _context.Audits.Add(new Audit
            {
                EventId = (int)EnumAuditEvents.RequestAdded,
                Description = EnumAuditEvents.RequestAdded.GetEnumDescription(),
                TimeStamp = DateTime.Now
            });

            await _context.SaveChangesAsync();

            return 1;
        }

        public async Task<int> ApproveUserRequest(string userId, List<int> requestId, string tempPassword)
        {
            foreach (var id in requestId)
            {
                var request = await _context.UserRequests.FirstAsync(x => x.Id == id);

                // create a new user from request
                var user = new ApplicationUser
                {
                    FirstName = request.FirstName,
                    Surname = request.Surname,
                    PhoneNumber = request.PhoneNumber,
                    Email = request.EmailAddress,
                    NormalizedEmail = request.EmailAddress.ToUpper(),
                    UserName = request.EmailAddress.Replace("@irissmart.com", "").ToLower(),
                    NormalizedUserName = request.EmailAddress.Replace("@irissmart.com", "").ToUpper(),
                    Gender = request.Gender,
                    DateCreated = DateTime.Now,
                    SecurityStamp = Guid.NewGuid().ToString("D")
                };

                if (!_context.Users.Any(u => u.Email == user.Email))
                {
                    // Add audit
                    _context.Audits.Add(new Audit
                    {
                        EventId = (int)EnumAuditEvents.RequestApproved,
                        Description = EnumAuditEvents.RequestApproved.GetEnumDescription(),
                        TimeStamp = DateTime.Now
                    });

                    var password = new PasswordHasher<ApplicationUser>();
                    var hashed = password.HashPassword(user, tempPassword);
                    user.PasswordHash = hashed;

                    var userStore = new UserStore<ApplicationUser>(_context);
                    userStore.CreateAsync(user).Wait();

                    // update the request as well
                    request.UserId = user.Id;
                    request.Approved = true;

                    await _context.SaveChangesAsync();
                }
            }

            return 1;
        }

        public async Task<int> RejectUserRequest(string userId, List<int> requestId)
        {
            foreach (var id in requestId)
            {
                var request = await _context.UserRequests.FirstAsync(x => x.Id == id);

                // Add audit
                _context.Audits.Add(new Audit
                {
                    EventId = (int)EnumAuditEvents.RequestDenied,
                    Description = EnumAuditEvents.RequestApproved.GetEnumDescription(),
                    TimeStamp = DateTime.Now,
                    UserId = null
                });

                // update the request as well
                request.Approved = false;

                await _context.SaveChangesAsync();
            }

            return 1;
        }

        public async Task<int> SaveError(Exception ex, string url, string userId)
        {
            _context.ErrorLogs.Add(new ErrorLog
            {
                UserId = userId,
                DateCreated = DateTime.Now,
                InnerException = ex.ToString(),
                Message = ex.Message,
                Type = ex.GetType().ToString(),
                Url = url
            });

            return await _context.SaveChangesAsync();
        }
    }
}
