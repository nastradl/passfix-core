﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using PassFix.Data;
using PassFix.Enums;
using PassFix.Extensions;
using PassFix.Models.Central;
using PassFix.Repositories;
using PassFix.ViewModels;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace PassFix.Services
{
    public class BranchService: IBranchRepository
    {
        private NgricaoDbContext _context;
        private readonly AuthDbContext _authContext;
        private readonly IConfiguration _configuration;
        private readonly string port = ",1433";

        public BranchService(NgricaoDbContext ctx, AuthDbContext auth, IConfiguration configuration)
        {
            _context = ctx;
            _authContext = auth;
            _configuration = configuration;
        }

        public async Task<List<Branch>> GetBranches()
        {
            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri("http://10.10.10.50:486");
                var response = await client.GetAsync("WebInvestigatorService.svc/getbranches");

                if (response.IsSuccessStatusCode)
                {
                    return JsonConvert.DeserializeObject<List<Branch>>(response.Content.ReadAsStringAsync().Result);

                }
            }

            return null;
        }

        public async Task<Record> SearchEnrolementByFormNo(string formno, string branch, string userId)
        {
            string connString = $"Server={ branch }{ port }; { _configuration["AppSettings:branchConnectionString"] }";

            // for now, only use local ip
            connString = "Server=.\\SQLExpress;Database=NGRICAO; User Id=idenuser; Password=idencraft;";
            _context = new NgricaoDbContextFactory().CreateDbContext(new []{ connString });

            // confirm the connection is valid
            //_context.Database.OpenConnection();
            //_context.Database.CloseConnection();

            var enroleProfile = await _context.LocEnrolProfile.SingleOrDefaultAsync(x => x.Formno == formno);
            var docHolderMainpProfile = await _context.LocDocHolderMainProfile.SingleOrDefaultAsync(x => x.Formno == formno);
            var docHolderCustomProfile = await _context.LocDocHolderCustomProfile.SingleOrDefaultAsync(x => x.Formno == formno);

            if (enroleProfile == null || docHolderMainpProfile == null || docHolderCustomProfile == null)
            {
                return null;
            }

            // if there is a docprofile, get the passport number
            string passportNo = string.Empty;
            string doctype = string.Empty;
            var docprofile = await _context.LocDocProfile.SingleOrDefaultAsync(x => x.Formno == formno);

            if (docprofile != null)
            {
                passportNo = docprofile.Docno;
                doctype = docprofile.Doctype;
            }

            // add audit
            _authContext.Audits.Add(new Audit
            {
                EventId = (int)EnumAuditEvents.SearchEnrolProfile,
                Description = EnumAuditEvents.SearchEnrolProfile.GetEnumDescription() + " - formno: " + formno,
                TimeStamp = DateTime.Now,
                UserId = userId
            });

            await _authContext.SaveChangesAsync();

            return new Record
            {
                FirstName = docHolderMainpProfile.Firstname,
                Surname = docHolderMainpProfile.Surname,
                MiddleName = docHolderCustomProfile.Othername1,
                FormNo = enroleProfile.Formno,
                StageCode = enroleProfile.Stagecode,
                BranchCode = branch,
                PassportNo = passportNo,
                PassportType = doctype,
                LocationId = enroleProfile.Enrollocationid
            };
        }

        public async Task<Record> SearchEnrolementByDocNo(string docno, string branch, string userId)
        {
            string connString = $"Server={ branch }{ port }; { _configuration["AppSettings:branchConnectionString"] }";

            // for now, only use local ip
            connString = "Server=.\\SQLExpress;Database=NGRICAO; User Id=idenuser; Password=idencraft;";
            _context = new NgricaoDbContextFactory().CreateDbContext(new[] { connString });

            // Confirm there is a docno
            var docprofile = await _context.LocDocProfile.SingleOrDefaultAsync(x => string.Equals(x.Docno, docno, StringComparison.CurrentCultureIgnoreCase));

            if (docprofile == null)
                return null;

            var formno = docprofile.Formno;
            var enroleProfile = await _context.LocEnrolProfile.SingleOrDefaultAsync(x => x.Formno == formno);
            var docHolderMainpProfile = await _context.LocDocHolderMainProfile.SingleOrDefaultAsync(x => x.Formno == formno);
            var docHolderCustomProfile = await _context.LocDocHolderCustomProfile.SingleOrDefaultAsync(x => x.Formno == formno);

            if (enroleProfile == null || docHolderMainpProfile == null || docHolderCustomProfile == null)
            {
                return null;
            }

            // add audit
            _authContext.Audits.Add(new Audit
            {
                EventId = (int)EnumAuditEvents.SearchEnrolProfile,
                Description = EnumAuditEvents.SearchEnrolProfile.GetEnumDescription() + " - formno: " + formno,
                TimeStamp = DateTime.Now,
                UserId = userId
            });

            await _authContext.SaveChangesAsync();

            return new Record
            {
                FirstName = docHolderMainpProfile.Firstname,
                Surname = docHolderMainpProfile.Surname,
                MiddleName = docHolderCustomProfile.Othername1,
                FormNo = enroleProfile.Formno,
                StageCode = enroleProfile.Stagecode,
                BranchCode = branch,
                PassportNo = docprofile.Docno,
                PassportType = docprofile.Doctype,
                LocationId = enroleProfile.Enrollocationid
            };
        }

        public async Task<DetailedRecord> SearchEnrolementByFormNoDetailed(string formno, string branch, string userId)
        {
            string connString = $"Server={ branch }{ port }; { _configuration["AppSettings:branchConnectionString"] }";

            // for now, only use local ip
            connString = "Server=.\\SQLExpress;Database=NGRICAO; User Id=idenuser; Password=idencraft;";
            _context = new NgricaoDbContextFactory().CreateDbContext(new[] { connString });

            // confirm the connection is valid
            //_context.Database.OpenConnection();
            //_context.Database.CloseConnection();

            var enroleProfile = await _context.LocEnrolProfile.SingleOrDefaultAsync(x => x.Formno == formno);
            var docHolderMainpProfile = await _context.LocDocHolderMainProfile.SingleOrDefaultAsync(x => x.Formno == formno);
            var docHolderCustomProfile = await _context.LocDocHolderCustomProfile.SingleOrDefaultAsync(x => x.Formno == formno);
            var paymentHistory = await _context.LocPaymentHistory.SingleOrDefaultAsync(x => x.Formno == formno);

            if (enroleProfile == null || docHolderMainpProfile == null || docHolderCustomProfile == null)
            {
                return null;
            }

            // add audit
            _authContext.Audits.Add(new Audit
            {
                EventId = (int)EnumAuditEvents.SearchEnrolProfile,
                Description = EnumAuditEvents.SearchEnrolProfile.GetEnumDescription() + " - formno: " + formno,
                TimeStamp = DateTime.Now,
                UserId = userId
            });

            await _authContext.SaveChangesAsync();

            return new DetailedRecord
            {
                FirstName = docHolderMainpProfile.Firstname,
                Surname = docHolderMainpProfile.Surname,
                MiddleName = docHolderCustomProfile.Othername1,
                FormNo = enroleProfile.Formno,
                StageCode = enroleProfile.Stagecode,
                BranchCode = branch,
                ApplicationId = paymentHistory.Appid,
                ReferenceId = paymentHistory.Refno,
                Appreason = enroleProfile.Appreason,
                DateOfBirth = docHolderMainpProfile.Birthdate,
                DocPage = enroleProfile.Docpage,
                DocType = enroleProfile.Doctype,
                Remarks = enroleProfile.Remarks,
                Nofinger = enroleProfile.Nofinger == 1,
                Title = docHolderCustomProfile.Title,
                OriginState = docHolderCustomProfile.Originstate,
                BirthState = docHolderMainpProfile.Birthstate,
                Sex = docHolderMainpProfile.Sex,
                BirthTown = docHolderMainpProfile.Birthtown
            };
        }

        public async Task<Record> SearchForApprovalRecord(string formno, string branch, string userId)
        {
            string connString = $"Server={ branch }{ port }; { _configuration["AppSettings:branchConnectionString"] }";

            // for now, only use local ip
            connString = "Server=.\\SQLExpress;Database=NGRICAO; User Id=idenuser; Password=idencraft;";
            _context = new NgricaoDbContextFactory().CreateDbContext(new[] { connString });

            var enroleProfile = await _context.LocEnrolProfile.SingleOrDefaultAsync(x => x.Formno == formno);
            var docHolderMainpProfile = await _context.LocDocHolderMainProfile.SingleOrDefaultAsync(x => x.Formno == formno);
            var docHolderCustomProfile = await _context.LocDocHolderCustomProfile.SingleOrDefaultAsync(x => x.Formno == formno);

            if (enroleProfile == null || docHolderMainpProfile == null || docHolderCustomProfile == null)
            {
                return null;
            }

            var result = new Record
            {
                FirstName = docHolderMainpProfile.Firstname,
                Surname = docHolderMainpProfile.Surname,
                MiddleName = docHolderCustomProfile.Othername1,
                FormNo = enroleProfile.Formno,
                StageCode = enroleProfile.Stagecode,
                BranchCode = branch,
                LocationId = enroleProfile.Enrollocationid
            };

            // if docprofile or persoprofile exist, return error
            var hasDocprofile = await _context.LocDocProfile.AnyAsync(x => x.Formno == formno);
            var hasPerso = await _context.LocPersoProfile.AnyAsync(x => x.Formno == formno);

            if (hasDocprofile)
                result.Error = "Cannot alter this record as it has a DocProfile";

            if (hasPerso)
            {
                if (hasDocprofile)
                    result.Error += " and a PersoProfile";
                else
                    result.Error = "Cannot alter this record as it has a PersoProfile";
            }
            
            if (!string.IsNullOrEmpty(result.Error))
                return result;

            // add audit
            _authContext.Audits.Add(new Audit
            {
                EventId = (int)EnumAuditEvents.SearchEnrolProfile,
                Description = EnumAuditEvents.SearchEnrolProfile.GetEnumDescription() + " - formno: " + formno,
                TimeStamp = DateTime.Now,
                UserId = userId
            });

            await _authContext.SaveChangesAsync();

            return result;
        }

        public async Task<int> UpdateAfisRecord(Record record, string userId)
        {
            string connString = $"Server={ record.BranchCode }{ port }; { _configuration["AppSettings:branchConnectionString"] }";

            // for now, only use local ip
            connString = "Server=.\\SQLExpress;Database=NGRICAO; User Id=idenuser; Password=idencraft;";
            _context = new NgricaoDbContextFactory().CreateDbContext(new[] { connString });
            
            var enroleProfile = await _context.LocEnrolProfile.SingleAsync(x => x.Formno == record.FormNo);
            if (enroleProfile.Stagecode == "EM2001")
            {
                enroleProfile.Stagecode = "EM1500";
                enroleProfile.Remarks = string.Empty;
            }
               

            var result = await _context.SaveChangesAsync();

            // add audit
            _authContext.Audits.Add(new Audit
            {
                EventId = (int)EnumAuditEvents.UpdateAfisRecord,
                Description = EnumAuditEvents.UpdateAfisRecord.GetEnumDescription() + " on FormNo " + record.FormNo,
                TimeStamp = DateTime.Now,
                UserId = userId
            });

            await _authContext.SaveChangesAsync();

            return result;
        }

        public async Task<int> UpdateBackToApproval(Record record, string userId)
        {
            string connString = $"Server={ record.BranchCode }{ port }; { _configuration["AppSettings:branchConnectionString"] }";

            // for now, only use local ip
            connString = "Server=.\\SQLExpress;Database=NGRICAO; User Id=idenuser; Password=idencraft;";
            _context = new NgricaoDbContextFactory().CreateDbContext(new[] { connString });

            var enroleProfile = await _context.LocEnrolProfile.SingleAsync(x => x.Formno == record.FormNo);

            // change the stage code
            if (enroleProfile.Stagecode == "EM4000")
                enroleProfile.Stagecode = "EM2000";

            // also set enrol location id
            enroleProfile.Enrollocationid = record.LocationId;

            var result = await _context.SaveChangesAsync();

            // add audit for enrol change
            _authContext.Audits.Add(new Audit
            {
                EventId = (int)EnumAuditEvents.UpdateBackToApproval,
                Description = EnumAuditEvents.UpdateBackToApproval.GetEnumDescription() + " on FormNo " + record.FormNo,
                TimeStamp = DateTime.Now,
                UserId = userId
            });
            
            await _authContext.SaveChangesAsync();

            return result;
        }

        public async Task<int> UpdateBackToPaymentCheck(Record record, string userId)
        {
            string connString = $"Server={ record.BranchCode }{ port }; { _configuration["AppSettings:branchConnectionString"] }";

            // for now, only use local ip
            connString = "Server=.\\SQLExpress;Database=NGRICAO; User Id=idenuser; Password=idencraft;";
            _context = new NgricaoDbContextFactory().CreateDbContext(new[] { connString });

            var enroleProfile = await _context.LocEnrolProfile.SingleAsync(x => x.Formno == record.FormNo);

            // change the stage code
            if (enroleProfile.Stagecode == "EM0801")
                enroleProfile.Stagecode = "EM0800";
            
            var result = await _context.SaveChangesAsync();

            // add audit for enrol change
            _authContext.Audits.Add(new Audit
            {
                EventId = (int)EnumAuditEvents.UpdateBackToPaymentCheck,
                Description = EnumAuditEvents.UpdateBackToPaymentCheck.GetEnumDescription() + " on FormNo " + record.FormNo,
                TimeStamp = DateTime.Now,
                UserId = userId
            });

            await _authContext.SaveChangesAsync();

            return result;
        }

        public async Task<int> UpdateBackToPerso(Record record, string userId)
        {
            string connString = $"Server={ record.BranchCode }{ port }; { _configuration["AppSettings:branchConnectionString"] }";

            // for now, only use local ip
            connString = "Server=.\\SQLExpress;Database=NGRICAO; User Id=idenuser; Password=idencraft;";
            _context = new NgricaoDbContextFactory().CreateDbContext(new[] { connString });

            var docProfile = await _context.LocDocProfile.SingleAsync(x => x.Formno == record.FormNo);
            var persoProfile = await _context.LocPersoProfile.SingleAsync(x => x.Formno == record.FormNo);

            // change the stage code
            if (docProfile.Stagecode == "PM2000")
                docProfile.Stagecode = "PM2001";

            if (persoProfile.Stagecode == "PM2000")
                persoProfile.Stagecode = "PM2001";

            await _context.SaveChangesAsync();

            // add audit for enrol change
            _authContext.Audits.Add(new Audit
            {
                EventId = (int)EnumAuditEvents.UpdateBackToPerso,
                Description = EnumAuditEvents.UpdateBackToPerso.GetEnumDescription() + " on FormNo " + record.FormNo,
                TimeStamp = DateTime.Now,
                UserId = userId
            });

            await _authContext.SaveChangesAsync();

            return 1;
        }

        public async Task<int> UpdateEnrolmentRecord(DetailedRecord record, string userId)
        {
            string connString = $"Server={ record.BranchCode }{ port }; { _configuration["AppSettings:branchConnectionString"] }";

            // for now, only use local ip
            connString = "Server=.\\SQLExpress;Database=NGRICAO; User Id=idenuser; Password=idencraft;";
            _context = new NgricaoDbContextFactory().CreateDbContext(new[] { connString });

            var enroleProfile = await _context.LocEnrolProfile.SingleOrDefaultAsync(x => x.Formno == record.FormNo);
            var docHolderMainpProfile = await _context.LocDocHolderMainProfile.SingleOrDefaultAsync(x => x.Formno == record.FormNo);
            var docHolderCustomProfile = await _context.LocDocHolderCustomProfile.SingleOrDefaultAsync(x => x.Formno == record.FormNo);
            var paymentHistory = await _context.LocPaymentHistory.SingleOrDefaultAsync(x => x.Formno == record.FormNo);

            // update records
            if (record.FirstName?.ToUpper() != docHolderMainpProfile.Firstname?.ToUpper().Trim())
            {
                docHolderMainpProfile.Firstname = record.FirstName?.ToUpper();
                _authContext.Audits.Add(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                    Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + " - Firstname",
                    TimeStamp = DateTime.Now,
                    UserId = userId
                });
            }

            if (record.Surname?.ToUpper() != docHolderMainpProfile.Surname?.ToUpper().Trim())
            {
                docHolderMainpProfile.Surname = record.Surname?.ToUpper();
                _authContext.Audits.Add(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                    Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + " - Surname",
                    TimeStamp = DateTime.Now,
                    UserId = userId
                });
            }

            if (record.DateOfBirth != docHolderMainpProfile.Birthdate)
            {
                docHolderMainpProfile.Birthdate = record.DateOfBirth;
                _authContext.Audits.Add(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                    Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + " - BirthDate",
                    TimeStamp = DateTime.Now,
                    UserId = userId
                });
            }

            if (record.Sex != docHolderMainpProfile.Sex)
            {
                docHolderMainpProfile.Sex = record.Sex;
                _authContext.Audits.Add(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                    Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + " - Sex",
                    TimeStamp = DateTime.Now,
                    UserId = userId
                });
            }

            if (record.BirthTown?.ToUpper() != docHolderMainpProfile.Birthtown?.ToUpper().Trim())
            {
                docHolderMainpProfile.Birthtown = record.BirthTown?.ToUpper();
                _authContext.Audits.Add(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                    Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + " - Birthtown",
                    TimeStamp = DateTime.Now,
                    UserId = userId
                });
            }

            if (record.BirthState?.ToUpper() != docHolderMainpProfile.Birthstate?.ToUpper().Trim())
            {
                docHolderMainpProfile.Birthstate = record.BirthState;
                _authContext.Audits.Add(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                    Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + " - Birthstate",
                    TimeStamp = DateTime.Now,
                    UserId = userId
                });
            }


            if (record.Title?.ToUpper() != docHolderCustomProfile.Title?.ToUpper().Trim())
            {
                docHolderCustomProfile.Title = record.Title?.ToUpper();
                _authContext.Audits.Add(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                    Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + " - Title",
                    TimeStamp = DateTime.Now,
                    UserId = userId
                });
            }

            if (record.MiddleName?.ToUpper() != docHolderCustomProfile.Othername1?.ToUpper().Trim())
            {
                docHolderCustomProfile.Othername1 = record.MiddleName?.ToUpper();
                _authContext.Audits.Add(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                    Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + " - Othername1",
                    TimeStamp = DateTime.Now,
                    UserId = userId
                });
            }

            if (record.OriginState?.ToUpper() != docHolderCustomProfile.Originstate?.ToUpper().Trim())
            {
                docHolderCustomProfile.Originstate = record.OriginState?.ToUpper();
                _authContext.Audits.Add(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                    Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + " - Originstate",
                    TimeStamp = DateTime.Now,
                    UserId = userId
                });
            }

          
            if (record.Appreason != enroleProfile.Appreason)
            {
                enroleProfile.Appreason = record.Appreason;
                _authContext.Audits.Add(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                    Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + " - Appreason",
                    TimeStamp = DateTime.Now,
                    UserId = userId
                });
            }

            if (record.DocPage != enroleProfile.Docpage)
            {
                enroleProfile.Docpage = record.DocPage;
                _authContext.Audits.Add(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                    Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + " - Docpage",
                    TimeStamp = DateTime.Now,
                    UserId = userId
                });
            }

            if (record.DocType != enroleProfile.Doctype)
            {
                enroleProfile.Doctype = record.DocType;
                _authContext.Audits.Add(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                    Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + " - Doctype",
                    TimeStamp = DateTime.Now,
                    UserId = userId
                });
            }

            if (record.Remarks != enroleProfile.Remarks?.Trim())
            {
                enroleProfile.Remarks = record.Remarks;
                _authContext.Audits.Add(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                    Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + " - Remarks",
                    TimeStamp = DateTime.Now,
                    UserId = userId
                });
            }

            if (record.Nofinger != (enroleProfile.Nofinger == 1))
            {
                enroleProfile.Nofinger = record.Nofinger ? 1 : 0;
                _authContext.Audits.Add(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                    Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + " - Nofinger",
                    TimeStamp = DateTime.Now,
                    UserId = userId
                });
            }


            if (record.ApplicationId != paymentHistory.Appid)
            {
                paymentHistory.Appid = record.ApplicationId;
                _authContext.Audits.Add(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                    Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + " - Appid",
                    TimeStamp = DateTime.Now,
                    UserId = userId
                });
            }

            if (record.ReferenceId != paymentHistory.Refno)
            {
                paymentHistory.Refno = record.ReferenceId;
                _authContext.Audits.Add(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                    Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + " - Refno",
                    TimeStamp = DateTime.Now,
                    UserId = userId
                });
            }

            await _context.SaveChangesAsync();

            await _authContext.SaveChangesAsync();

            return 1;
        }
    }
}
