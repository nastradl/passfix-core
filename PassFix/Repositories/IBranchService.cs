﻿using PassFix.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PassFix.Repositories
{
    public interface IBranchRepository
    {
        Task<List<Branch>> GetBranches();

        Task<Record> SearchEnrolementByFormNo(string form, string branch, string userId);

        Task<Record> SearchEnrolementByDocNo(string docno, string branch, string userId);

        Task<DetailedRecord> SearchEnrolementByFormNoDetailed(string form, string branch, string userId);

        Task<Record> SearchForApprovalRecord(string form, string branch, string userId);

        Task<int> UpdateAfisRecord(Record record, string userId);

        Task<int> UpdateBackToApproval(Record record, string userId);

        Task<int> UpdateBackToPaymentCheck(Record record, string userId);

        Task<int> UpdateBackToPerso(Record record, string userId);

        Task<int> UpdateEnrolmentRecord(DetailedRecord record, string userId);
    }
}
