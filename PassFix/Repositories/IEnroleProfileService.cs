﻿using PassFix.Enums;
using PassFix.Models.NGRICAO;
using System.Threading.Tasks;

namespace PassFix.Repositories
{
    public interface IEnrolProfileService
    {
        Task<LocEnrolProfile> GetEnrolProfile(string formNo);

        Task<int> UpdateEnrolProfile(LocEnrolProfile enrolProfile);

        Task<int> ChangeStageCode(EnumStageCodes from, EnumStageCodes to);
    }
}
