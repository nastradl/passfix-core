﻿function Amu() {
    SetSemanticUiInputs();

    $(".searchform")
        .form({
            className: { success: "" },
            fields: {
                formno: {
                    identifier: "formno",
                    rules: [
                        {
                            type: "empty",
                            prompt: "Please enter search details"
                        }
                    ]
                },
                searchbranchcode: {
                    identifier: "searchbranchcode",
                    rules: [
                        {
                            type: "empty",
                            prompt: "Please select a branch"
                        }
                    ]
                }
            },
            onSuccess: function (event) {
                $(this).find(".submit").addClass("loading");
            }
        })
        .api({
            serializeForm: true,
            method: "POST", 
            onSuccess: function (response) {
                // clear any previous error
                $(this).find(".error .header").text();

                // remove loading
                $(this).find(".submit").removeClass("loading");

                var searchResults = $("#searchresults").html(response.page);

                // update page
                searchResults.html(response.page);

                if (response.error) {
                    if (response.badstagecode) {
                        $(".stagecodediv").addClass("redbg");
                    }

                    var errors = [response.error];
                    $(this).form("add errors", errors);
                }
                else {
                    $(".stagecodediv").removeClass("redbg");

                    // enable anything that requires it
                    if (response.enable) {
                        $(".ui.disabled").removeClass("disabled");
                        $(".ui.perma").addClass("disabled");
                    }

                    // allow update now
                    OnUpdate();

                    // enable update button
                    searchResults.find(".submit").removeClass("disabled");
                }

                // refresh semantic ui
                SetSemanticUiInputs();
            },
            onFailure: function (response) {
                // remove loading
                $(this).find(".submit").removeClass("loading");

                // disable update button
                $(this).siblings("form").find(".submit").removeClass("disabled");

                if (response.success != 0) {
                    showError(null, null, msgType.Error);
                } else {
                    if (response.error) {
                        var errors = [response.error];
                        $(this).form("add errors", errors);
                        ClearForm();
                    }
                }
            }
        });
}

function OnUpdate() {
    $(".update.form")
        .form({
            className: { success: "" }
        })
        .api({
            serializeForm: true,
            method: "POST",
            onSuccess: function (response) {
                var form = $(this);

                // show success message
                form.addClass("success");

                // clear any previous error
                form.find(".field.error").removeClass("error");
                form.find(".ui.error.message ul").remove();

                // after a few seconds, clear form
                setTimeout(
                    function () {
                        if (response.clearform) {
                            ClearForm();
                        }

                        form.removeClass("success");
                    },
                    2000);
            },
            onFailure: function (response) {
                if (response.success != 0) {
                    showError(null, null, msgType.Error);
                } else {
                    if (response.error) {
                        var errors = [response.error];
                        $(this).form("add errors", errors);
                    }
                }
            }
        });
}

function SetSemanticUiInputs() {
    $(".ui.dropdown")
        .dropdown();

    $(".ui.checkbox")
        .checkbox();

    // switch between passport search and formno
    $(".ui.searchdoc")
        .checkbox().checkbox({
            onChecked: function () {
                // clear any previous error
                $(".searchform").removeClass("error").form("clear");

                $("#searchbyform").transition({
                    animation: "fade right",
                    onComplete: function () {
                        $("#searchbydoc").transition("fade right");
                        $("#FormNo").val("");
                    }
                });
            },
            onUnchecked: function () {
                // clear any previous error
                $(".searchform").removeClass("error").form("clear");

                $("#searchbydoc").transition({
                    animation: "fade right",
                    onComplete: function () {
                        $("#searchbyform").transition("fade right");
                        $("#DocNo").val("");
                    }
                });
            }
        });
}

function ClearForm() {
    // replace branch selected after clear
    var branch = $("#searchbranchcode").val();
    $("form").form("clear");
    $(".clearable").text("");
    $(".searchbranchcode").val(branch);

    // disable button
    $("#updateform").find(".submit").addClass("disabled").text("Update");
}