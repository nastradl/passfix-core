﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PassFix.Data;
using PassFix.Extensions;
using PassFix.Helpers;
using PassFix.Models;
using PassFix.Repositories;
using PassFix.Services;
using System;
using PassFix.Models.Central;

namespace PassFix
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<AuthDbContext>()
                .AddDefaultTokenProviders();

            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 4;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
            });

            services.AddDbContext<NgricaoDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("NGRICAO")));

            services.AddDbContext<AuthDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("Central")));

            services.AddMvc()
                .AddRazorPagesOptions(options =>
                {
                    options.Conventions.AuthorizeFolder("/Account/Manage");
                    options.Conventions.AuthorizePage("/Account/Logout");
                });

            services.AddTransient<DbContextExtensions>();
            services.AddSingleton(Configuration);
            services.AddScoped<IEnrolProfileService, EnroleProfileService>();
            services.AddScoped<IEmailSender, EmailSender>();
            services.AddScoped<IViewRenderService, ViewRenderService>();
            services.AddScoped<IBranchRepository, BranchService>();
            services.AddScoped<IAuthRepository, AuthService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, DbContextExtensions SeedData)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseMiddleware(typeof(ErrorHandlingMiddleware));

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

                routes.MapRoute(
                    name: "tools",
                    template: "{action}/{id?}",
                    defaults: new { controller = "Home", action = "Tools" });

               
            });

            // Create default user password
            var defaultAdminPw = Configuration["SeedUserPW"];
            if (string.IsNullOrEmpty(defaultAdminPw))
            {
                throw new Exception("Use secrets manager to set SeedUserPW 'dotnet user-secrets set SeedUserPW <pw>'");
            }

            try
            {
                // Add seed data
                SeedData.Seed(app.ApplicationServices, defaultAdminPw);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
