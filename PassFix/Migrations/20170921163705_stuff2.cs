﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace PassFix.Migrations
{
    public partial class stuff2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Audits_AuditEvents_EventId",
                table: "Audits");

            migrationBuilder.DropPrimaryKey(
                name: "PK_UserClaims",
                table: "UserClaims");

            migrationBuilder.DropPrimaryKey(
                name: "PK_States",
                table: "States");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AuditEvents",
                table: "AuditEvents");

            migrationBuilder.RenameTable(
                name: "UserClaims",
                newName: "ListUserClaims");

            migrationBuilder.RenameTable(
                name: "States",
                newName: "ListStates");

            migrationBuilder.RenameTable(
                name: "AuditEvents",
                newName: "ListAuditEvents");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ListUserClaims",
                table: "ListUserClaims",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ListStates",
                table: "ListStates",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ListAuditEvents",
                table: "ListAuditEvents",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "ListTitles",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ListTitles", x => x.Id);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_Audits_ListAuditEvents_EventId",
                table: "Audits",
                column: "EventId",
                principalTable: "ListAuditEvents",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Audits_ListAuditEvents_EventId",
                table: "Audits");

            migrationBuilder.DropTable(
                name: "ListTitles");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ListUserClaims",
                table: "ListUserClaims");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ListStates",
                table: "ListStates");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ListAuditEvents",
                table: "ListAuditEvents");

            migrationBuilder.RenameTable(
                name: "ListUserClaims",
                newName: "UserClaims");

            migrationBuilder.RenameTable(
                name: "ListStates",
                newName: "States");

            migrationBuilder.RenameTable(
                name: "ListAuditEvents",
                newName: "AuditEvents");

            migrationBuilder.AddPrimaryKey(
                name: "PK_UserClaims",
                table: "UserClaims",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_States",
                table: "States",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AuditEvents",
                table: "AuditEvents",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Audits_AuditEvents_EventId",
                table: "Audits",
                column: "EventId",
                principalTable: "AuditEvents",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
