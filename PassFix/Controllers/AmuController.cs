﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PassFix.Helpers;
using PassFix.Models.Central;
using PassFix.Repositories;
using PassFix.Services;
using PassFix.ViewModels;
using System.Threading.Tasks;

namespace PassFix.Controllers
{
    [Authorize]
    public class AmuController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IViewRenderService _viewRenderService;
        private readonly IBranchRepository _rep;
        private readonly IAuthRepository _auth;

        public AmuController(UserManager<ApplicationUser> userManager,
            IBranchRepository rep,
            IAuthRepository auth,
            IViewRenderService viewRenderService)
        {
            _rep = rep;
            _auth = auth;
            _userManager = userManager;
            _viewRenderService = viewRenderService;
        }

        [ClaimRequirement("Amu", "EditEnrolmentRecord")]
        public async Task<IActionResult> EditEnrolmentRecord()
        {
            var states = await _auth.GetStates();
            var titles = await _auth.GetTitles();
            var model = new DetailedRecord
            {
                SearchUrl = "SearchEnrolmentRecord",
                PageTitle = "Edit Enrolment Record",
                States = states,
                Titles = titles
            };

            return Json(new
            {
                success = 1,
                page = await _viewRenderService.RenderToStringAsync("Amu/_DetailedBaseUpdateView", model)
            });
        }

        [ClaimRequirement("Amu", "BackToAfisCheck")]
        public async Task<IActionResult> BackToAfisCheck()
        {
            var model = new Record
            {
                SearchUrl = "SearchBackToAfisCheck",
                Title = "Back To Afis Check"
            };

            return Json(new
            {
                success = 1,
                page = await _viewRenderService.RenderToStringAsync("Amu/_BaseUpdateView", model)
            });
        }

        [ClaimRequirement("Amu", "BackToApproval")]
        public async Task<IActionResult> BackToApproval()
        {
            var model = new Record
            {
                SearchUrl = "SearchBackToApproval",
                UpdateUrl = "UpdateBackToApproval",
                Title = "Back To Approval",
                AllowEditLocation = true
            };

            return Json(new
            {
                success = 1,
                page = await _viewRenderService.RenderToStringAsync("Amu/_BaseUpdateView", model)
            });
        }

        [ClaimRequirement("Amu", "BackToPerso")]
        public async Task<IActionResult> BackToPerso()
        {
            var model = new Record
            {
                SearchUrl = "SearchBackToPerso",
                UpdateUrl = "UpdateBackToPerso",
                Title = "Back To Perso",
                ShowPassportNo = true,
                ShowPassportType = true,
                ShowSearchByDocNo = true,
                AllowEditLocation = true
            };

            return Json(new
            {
                success = 1,
                page = await _viewRenderService.RenderToStringAsync("Amu/_BaseUpdateView", model)
            });
        }

        [ClaimRequirement("Amu", "BackToPaymentCheck")]
        public async Task<IActionResult> BackToPaymentCheck()
        {
            var model = new Record
            {
                SearchUrl = "SearchBackToPaymentCheck",
                UpdateUrl = "UpdateBackToPayment",
                Title = "Back To Payment"
            };

            return Json(new
            {
                success = 1,
                page = await _viewRenderService.RenderToStringAsync("Amu/_BaseUpdateView", model)
            });
        }

        [ClaimRequirement("Amu", "IssuePassport")]
        public async Task<IActionResult> IssuePassport()
        {

            return Json(new
            {
                success = 1,
                page = await _viewRenderService.RenderToStringAsync("Amu/_IssuePassport", new Record())
            });
        }

        [ClaimRequirement("Amu", "QueryBookletInfo")]
        public async Task<IActionResult> QueryBookletInfo()
        {

            return Json(new
            {
                success = 1,
                page = await _viewRenderService.RenderToStringAsync("Amu/_QueryBookletInfo", new Record())
            });
        }

        [ClaimRequirement("Amu", "OverideBookletSequence")]
        public async Task<IActionResult> OverideBookletSequence()
        {

            return Json(new
            {
                success = 1,
                page = await _viewRenderService.RenderToStringAsync("Amu/_OverideBookletSequence", new Record())
            });
        }

        //Json

        [ClaimRequirement("Amu", "BackToAfisCheck")]
        public async Task<IActionResult> SearchBackToAfisCheck(string formno, string searchbranchcode)
        {
            var record = await _rep.SearchEnrolementByFormNo(formno, searchbranchcode, _userManager.GetUserId(User));

            if (record == null)
                return Json(new
                {
                    success = 0,
                    error = "No record found matching your request"
                });

            // add update url
            record.UpdateUrl = "UpdateBackToAfisCheck";

            if (record.StageCode != "EM2001")
                return Json(new
                {
                    success = 1,
                    badstagecode = true,
                    page = await _viewRenderService.RenderToStringAsync("Amu/_BaseUpdateViewResult", record),
                    error = $"Cannot alter this record. It has a stagecode of {record.StageCode} instead of the required EM2001"
                });

            record.UpdateMsg = "Update to EM1500";
            return Json(new
            {
                success = 1,
                page = await _viewRenderService.RenderToStringAsync("Amu/_BaseUpdateViewResult", record)
            });
        }

        [ClaimRequirement("Amu", "BackToAfisCheck")]
        public async Task<IActionResult> UpdateBackToAfisCheck(Record record)
        {
            var result = await _rep.UpdateAfisRecord(record, _userManager.GetUserId(User));

            return Json(new
            {
                success = result,
                clearform = true
            });
        }


        [ClaimRequirement("Amu", "BackToApproval")]
        public async Task<IActionResult> SearchBackToApproval(string formno, string searchbranchcode)
        {
            var record = await _rep.SearchForApprovalRecord(formno, searchbranchcode, _userManager.GetUserId(User));


            if (record == null)
                return Json(new
                {
                    success = 0,
                    error = "No record found matching your request"
                });

            // add update url
            record.UpdateUrl = "UpdateBackToApproval";
            record.AllowEditLocation = true;
            string error = string.Empty;
            bool badstagecode = false;

            if (!string.IsNullOrEmpty(record.Error))
                error = record.Error;

            if (record.StageCode != "EM4000")
            {
                badstagecode = true;
                error =
                    $"Cannot alter this record. It has a stagecode of {record.StageCode} instead of the required EM4000";
            }

            if (string.IsNullOrEmpty(error))
            {
                record.UpdateMsg = "Update to EM2000";
                return Json(new
                {
                    success = 1,
                    enable = true,
                    page = await _viewRenderService.RenderToStringAsync("Amu/_BaseUpdateViewResult", record)
                });
            }

            return Json(new
                {
                    success = 1,
                    error,
                    badstagecode,
                    page = await _viewRenderService.RenderToStringAsync("Amu/_BaseUpdateViewResult", record)
                });
        }

        [ClaimRequirement("Amu", "BackToApproval")]
        public async Task<IActionResult> UpdateBackToApproval(Record record)
        {
            var result = await _rep.UpdateBackToApproval(record, _userManager.GetUserId(User));

            return Json(new
            {
                success = result,
                clearform = true
            });
        }


        [ClaimRequirement("Amu", "BackToPerso")]
        public async Task<IActionResult> SearchBackToPerso(string docno, string formno, string searchbranchcode)
        {
            var record = new Record();
            
            if (!string.IsNullOrEmpty(formno))
                record = await _rep.SearchEnrolementByFormNo(formno, searchbranchcode, _userManager.GetUserId(User));

            if (!string.IsNullOrEmpty(docno))
                record = await _rep.SearchEnrolementByDocNo(docno, searchbranchcode, _userManager.GetUserId(User));

            if (record == null)
                return Json(new
                {
                    success = 0,
                    error = "No record found matching your request"
                });

            // add update url
            record.UpdateUrl = "UpdateBackToPerso";
            record.AllowEditLocation = true;

            if (record.StageCode != "PM2000")
                return Json(new
                {
                    success = 1,
                    badstagecode = true,
                    page = await _viewRenderService.RenderToStringAsync("Amu/_BaseUpdateViewResult", record),
                    error = $"Cannot alter this record. It has a stagecode of {record.StageCode} instead of the required PM2000"
                });

            record.ShowPassportNo = true;
            record.ShowPassportType = true;
            record.UpdateMsg = "Update to PM2001";
            return Json(new
            {
                success = 1,
                enable = true,
                page = await _viewRenderService.RenderToStringAsync("Amu/_BaseUpdateViewResult", record)
            });
        }

        [ClaimRequirement("Amu", "BackToPerso")]
        public async Task<IActionResult> UpdateBackToPerso(Record record)
        {
            var result = await _rep.UpdateBackToPerso(record, _userManager.GetUserId(User));

            return Json(new
            {
                success = result,
                clearform = true
            });
        }


        [ClaimRequirement("Amu", "BackToPaymentCheck")]
        public async Task<IActionResult> SearchBackToPaymentCheck(string formno, string searchbranchcode)
        {
            var record = await _rep.SearchEnrolementByFormNo(formno, searchbranchcode, _userManager.GetUserId(User));

            if (record == null)
                return Json(new
                {
                    success = 0,
                    error = "No record found matching your request"
                });

            // add update url
            record.UpdateUrl = "UpdateBackToPaymentCheck";

            if (record.StageCode != "EM0801")
                return Json(new
                {
                    success = 1,
                    badstagecode = true,
                    page = await _viewRenderService.RenderToStringAsync("Amu/_BaseUpdateViewResult", record),
                    error = $"Cannot alter this record. It has a stagecode of {record.StageCode} instead of the required EM0801"
                });

            record.UpdateMsg = "Update to EM0800";
            return Json(new
            {
                success = 1,
                page = await _viewRenderService.RenderToStringAsync("Amu/_BaseUpdateViewResult", record)
            });
        }

        [ClaimRequirement("Amu", "BackToPaymentCheck")]
        public async Task<IActionResult> UpdateBackToPaymentCheck(Record record)
        {
            var result = await _rep.UpdateBackToPaymentCheck(record, _userManager.GetUserId(User));

            return Json(new
            {
                success = result,
                clearform = true
            });
        }


        [ClaimRequirement("Amu", "EditEnrolmentRecord")]
        public async Task<IActionResult> SearchEnrolmentRecord(string formno, string searchbranchcode)
        {
            var record = await _rep.SearchEnrolementByFormNoDetailed(formno, searchbranchcode, _userManager.GetUserId(User));

            if (record == null)
                return Json(new
                {
                    success = 0,
                    error = "No record found matching your request"
                });

            // add update url
            record.States = await _auth.GetStates();
            record.Titles = await _auth.GetTitles();
            record.UpdateUrl = "UpdateEnrolmentRecord";

            if (record.StageCode == "EM5000")
                return Json(new
                {
                    success = 1,
                    badstagecode = true,
                    page = await _viewRenderService.RenderToStringAsync("Amu/_DetailedBaseUpdateViewResult", record),
                    error = $"Cannot alter this record. It has a stagecode of {record.StageCode}"
                });

            return Json(new
            {
                success = 1,
                enable = true,
                page = await _viewRenderService.RenderToStringAsync("Amu/_DetailedBaseUpdateViewResult", record)
            });
        }

        [ClaimRequirement("Amu", "EditEnrolmentRecord")]
        public async Task<IActionResult> UpdateEnrolmentRecord(DetailedRecord record)
        {
            var result = await _rep.UpdateEnrolmentRecord(record, _userManager.GetUserId(User));

            return Json(new
            {
                success = result,
                clearform = false
            });
        }
    }
}
