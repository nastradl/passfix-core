﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using PassFix.Extensions;
using PassFix.Models;
using PassFix.Models.AccountViewModels;
using PassFix.Repositories;
using PassFix.Services;
using PassFix.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using PassFix.Models.Central;

namespace PassFix.Controllers
{
    [Authorize]
    [Route("[controller]/[action]")]
    public class AccountController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IBranchRepository _branch;
        private readonly IConfiguration _configuration;
        private readonly IViewRenderService _viewRenderService;
        private readonly IAuthRepository _rep;
        private readonly IEmailSender _emailSender;
        private readonly ILogger _logger;

        public AccountController(
            IAuthRepository rep,
            IViewRenderService viewRenderService,
            IConfiguration configuration,
            IBranchRepository branch,
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IEmailSender emailSender,
            ILogger<AccountController> logger)
        {
            _rep = rep;
            _viewRenderService = viewRenderService;
            _branch = branch;
            _userManager = userManager;
            _configuration = configuration;
            _signInManager = signInManager;
            _emailSender = emailSender;
            _logger = logger;
        }

        [TempData]
        public string ErrorMessage { get; set; }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                // This doesn't count login failures towards account lockout
                // To enable password failures to trigger account lockout, set lockoutOnFailure: true
                var signedUser = await _userManager.FindByEmailAsync(model.Email);

                if (signedUser == null)
                {
                    // Check if user used username instead
                    signedUser = await _userManager.FindByNameAsync(model.Email);

                    if (signedUser == null)
                        return Json(new
                        {
                            success = 0,
                            error = "Invalid username or password"
                        });
                }

                if (signedUser.LockoutEnabled)
                {
                    return Json(new
                    {
                        success = 0,
                        error = "Your account has been locked or disabled. Please contact an admin to re-enable it."
                    });
                }

                var result = await _signInManager.PasswordSignInAsync(signedUser.UserName, model.Password, model.RememberMe, lockoutOnFailure: false);

                if (result.Succeeded)
                {
                    _logger.LogInformation("User logged in.");

                    // Get user claims
                    var claims = await _userManager.GetClaimsAsync(signedUser);

                    // Get branches
                    var branches = new List<Branch>();
                    if (claims.Any(x => x.Type.ToLower() == "amu"))
                        branches = await _branch.GetBranches();

                    var menuModel = new MenuModel
                    {
                        Username = signedUser.UserName,
                        Claims = claims.OrderBy(x => x.Value).ToList(),
                        Branches = branches
                    };

                    return Json(new
                    {
                        success = 1,
                        homepage = await _viewRenderService.RenderToStringAsync("Home/Tools", menuModel),
                        menu = await _viewRenderService.RenderToStringAsync("_SideMenu", menuModel),
                        returnUrl
                    });
                }
               
                if (result.IsLockedOut)
                {
                    _logger.LogWarning("User account locked out.");
                    return Json(new
                    {
                        success = 0,
                        error = "You account has been locked",
                        returnUrl
                    });
                }

                return Json(new
                {
                    success = 0,
                    error = "Invalid username or password"
                });
            }

            return Json(new
            {
                success = 0,
                errors = ModelState.ToDictionary(
                    kvp => kvp.Key,
                    kvp => kvp.Value.Errors.Select(e => e.ErrorMessage).ToArray()
                )
            });
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(UserRequest request, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                // Check if this email is already registered/requested
                var exists = await _rep.CheckEmailRegistered(request.EmailAddress);

                if (exists)
                    return Json(new
                    {
                        success = 0,
                        error = "You are already registered on this platform. Please contact an admin if you encountered an issue while registering"
                    });

                var result = await _rep.AddUserRequest(request);

                return Json(new
                {
                    success = result
                }); 
            }

            return Json(new
            {
                success = 0,
                errors = ModelState.Where(x => x.Value.Errors.Count > 0).ToDictionary(
                    kvp => kvp.Key,
                    kvp => kvp.Value.Errors.Select(e => e.ErrorMessage).ToArray()
                )
            });
        }

        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            _logger.LogInformation("User logged out.");
            return RedirectToAction(nameof(HomeController.Index), "Home");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);
                if (user == null)
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return Json(new
                    {
                        success = 0,
                        error = "Sorry, no registered user with that email address"
                    });
                }

                var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                var callbackUrl = Url.ResetPasswordCallbackLink(user.Id, code, Request.Scheme);

                var iris = _configuration["AppSettings:companyEmail"];
                var friendlyName = _configuration["AppSettings:friendlyName"];
                var emailcredentials = _configuration["AppSettings:email"];
                var emailpassword = _configuration["AppSettings:password"];
                var styledBtn = "style='font-size: 16px;background-color: #027bbb;color: white;text-decoration: none;padding: 7px 20px;margin: 7px 0px;display: " +
                                "inline-block;-webkit-box-shadow: 2px 2px 5px rgba(0,0,0,0.3);-moz-box-shadow: 2px 2px 5px rgba(0,0,0,0.3);" +
                                "box-shadow: 2px 2px 5px rgba(0,0,0,0.3);border-radius: 5px;'";
                string subject = "PassFix password reset";
                string message = $@"<div>Hello { user.FirstName.ToTitleCase() } { user.Surname.ToTitleCase() },</div>
                                    <div style='margin-top: 10px'>A request was made to reset your password on the PassFix application.</div>
                                    <div style='margin-top: 10px'>Please click on the button below to reset your password:</div>
                                    <div style='margin-top: 10px'>
                                        <a href='{ callbackUrl }' {styledBtn}>Reset My Password</a>
                                    <div style='margin-top: 10px'>If you did not make this request please contact an administrator.</div>                                                                
                                    <div style='margin-top: 10px'>Thank you</div>";

                var mailMessage = new MailMessage
                {
                    From = new MailAddress(iris, friendlyName),
                    IsBodyHtml = true
                };
                mailMessage.To.Add(user.Email);
                mailMessage.Body = message;
                mailMessage.Subject = subject;

                using (var client = new SmtpClient("smtp.gmail.com", 587)
                {
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(emailcredentials, emailpassword)
                })
                {
                    client.Send(mailMessage);

                    await _emailSender.SendEmailAsync(model.Email, "Reset Password",
                        $"Please reset your password by clicking here: <a href='{callbackUrl}'>link</a>");

                    return Json(new
                    {
                        success = 1
                    });
                }
            }

            return Json(new
            {
                success = 0,
                error = "Invalid email address"
            });
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{userId}'.");
            }
            var result = await _userManager.ConfirmEmailAsync(user, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPassword(string code = null)
        {
            if (code == null)
            {
                throw new ApplicationException("A code must be supplied for password reset.");
            }
            var model = new ResetPasswordViewModel { Code = code };
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new
                {
                    success = 0,
                    errors = ModelState.ToDictionary(
                        kvp => kvp.Key,
                        kvp => kvp.Value.Errors.Select(e => e.ErrorMessage).ToArray()
                    )
                });
            }
            var user = await _userManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                return Json(new
                {
                    success = 0,
                    error = "Invalid credentials"
                });
            }

            var result = await _userManager.ResetPasswordAsync(user, model.Code, model.Password);
            if (result.Succeeded)
            {
                return Json(new
                {
                    success = 1
                });
            }

            return Json(new
            {
                success = 0
            });
        }
    }
}
