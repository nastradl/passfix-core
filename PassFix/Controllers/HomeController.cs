﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PassFix.Models;
using PassFix.Repositories;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using PassFix.Models.Central;
using PassFix.ViewModels;

namespace PassFix.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IAuthRepository _rep;
        private readonly IBranchRepository _branch;

        public HomeController(IAuthRepository rep, IBranchRepository branch, UserManager<ApplicationUser> userManager)
        {
            _rep = rep;
            _branch = branch;
            _userManager = userManager;
        }

        [AllowAnonymous]
        public IActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
                return RedirectToAction("Tools");

            return View();
        }

        public async Task<IActionResult> Tools(string section, string option)
        {
            var signedUser = await _userManager.GetUserAsync(User);

            // Get user claims
            var claims = await _userManager.GetClaimsAsync(signedUser);

            // Get branches
            var branches = new List<Branch>();
            if (claims.Any(x => x.Type.ToLower() == "amu"))
                branches = await _branch.GetBranches();

            var model = new MenuModel
            {
                Username = signedUser.UserName,
                Claims = claims.OrderBy(x => x.Value).ToList(),
                Branches = branches
            };

            return View(model);
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
