﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using PassFix.Extensions;
using PassFix.Helpers;
using PassFix.Models;
using PassFix.Repositories;
using PassFix.Services;
using PassFix.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using PassFix.Models.Central;

namespace PassFix.Controllers
{
    public class UserManagementController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IViewRenderService _viewRenderService;
        private readonly IAuthRepository _rep;
        private readonly IConfiguration _configuration;

        public UserManagementController(IAuthRepository rep,
            UserManager<ApplicationUser> userManager,
            IConfiguration configuration,
            IViewRenderService viewRenderService)
        {
            _rep = rep;
            _configuration = configuration;
            _userManager = userManager;
            _viewRenderService = viewRenderService;
        }

        [ClaimRequirement("UserManagement", "ManageMyAccount")]
        public async Task<IActionResult> ManageMyAccount()
        {
            var user = await _userManager.GetUserAsync(User);

            return Json(new
            {
                success = 1,
                page = await _viewRenderService.RenderToStringAsync("UserManagement/_ManageMyAccount", user)
            });
        }

        [ClaimRequirement("UserManagement", "ManageOtherUsers")]
        public async Task<IActionResult> ManageOtherUsers()
        {
            var users = (await _rep.GetUsers()).Where(x => x.Id != _userManager.GetUserId(User)).ToList();

            return Json(new
            {
                success = 1,
                page = await _viewRenderService.RenderToStringAsync("UserManagement/_ManageOtherUsersWrap", users)
            });
        }

        [ClaimRequirement("UserManagement", "ManageUserRequests")]
        public async Task<IActionResult> ManageUserRequests()
        {
            var requests = await _rep.GetUserRequests();

            return Json(new
            {
                success = 1,
                page = await _viewRenderService.RenderToStringAsync("UserManagement/_ManageUserRequests", requests)
            });
        }


        // Json
        [ClaimRequirement("UserManagement", "ManageUserRequests")]
        public async Task<IActionResult> ApproveUserRequest(string selected, string reject)
        {
            bool rejection = reject == "1";

            if (string.IsNullOrEmpty(selected))
                return Json(new
                {
                    success = 0,
                    error = "No users found matching request"
                });

            // convert selected to list of ids
            var ids = selected.Split(',').Select(int.Parse).ToList();
            var emailcredentials = _configuration["AppSettings:email"];
            var emailpassword = _configuration["AppSettings:password"];

            using (var client = new SmtpClient("smtp.gmail.com", 587)
            {
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(emailcredentials, emailpassword)
            })
            {
                if (rejection)
                {
                    foreach (var req in ids)
                    {
                        var request = await _rep.GetUserRequest(req);

                        await _rep.RejectUserRequest(_userManager.GetUserId(User), ids);

                        // send an email to user with registration information
                        var iris = _configuration["AppSettings:companyEmail"];
                        var friendlyName = _configuration["AppSettings:friendlyName"];
                        string subject = "PassFix registration request";
                        string message = $@"<div>Hello {request.FirstName.ToTitleCase()} {request.Surname.ToTitleCase()},</div>
                                    <div style='margin-top: 10px'>Unfortunately, your request to register with the Iris PassFix tool has been denied. 
                                    Please contact an admin staff if you need to discuss this decision.</div>
                                    <div style='margin-top: 10px'>Thank you</div>";

                        var mailMessage = new MailMessage
                        {
                            From = new MailAddress(iris, friendlyName),
                            IsBodyHtml = true
                        };
                        mailMessage.To.Add(request.EmailAddress);
                        mailMessage.Body = message;
                        mailMessage.Subject = subject;
                        client.Send(mailMessage);
                    }
                }
                else
                {
                    // send an email to user with registration information
                    foreach (var req in ids)
                    {
                        var request = await _rep.GetUserRequest(req);
                        string tempPassword = Helper.GenerateRandomString(6);
                        var result = await _rep.ApproveUserRequest(_userManager.GetUserId(User), ids, tempPassword);

                        if (result == -1)
                            return Json(new
                            {
                                success = 0,
                                error = "This user already exists in the database"
                            });
                        
                        var iris = _configuration["AppSettings:companyEmail"];
                        var friendlyName = _configuration["AppSettings:friendlyName"];
                        string subject = "PassFix registration request";
                        string message = $@"<div>Hello {request.FirstName.ToTitleCase()} {request.Surname.ToTitleCase()},</div>
                                    <div style='margin-top: 10px'>Your request to register with the Iris PassFix tool has been approved. 
                                    Please log in with your email and this temporary password:</div>
                                    <div style='font-size:18px; font-weight: bold; margin: 10px 0px'>{tempPassword}</div>
                                    <div>You will be required to create your own password the first time you log in.</div>
                                    <div style='margin-top: 10px'>Thank you</div>";

                        var mailMessage = new MailMessage
                        {
                            From = new MailAddress(iris, friendlyName),
                            IsBodyHtml = true
                        };
                        mailMessage.To.Add(request.EmailAddress);
                        mailMessage.Body = message;
                        mailMessage.Subject = subject;
                        client.Send(mailMessage);
                    }
                }
            }
                
            // get updated list of requests
            var requests = await _rep.GetUserRequests();
            return Json(new
            {
                success = 1,
                page = await _viewRenderService.RenderToStringAsync("UserManagement/_ManageUserRequests", requests)
            });
        }

        [ClaimRequirement("UserManagement", "ManageOtherUsers")]
        public async Task<IActionResult> GetUserPermissions(string id)
        {
            return Json(new
            {
                success = 1,
                page = await _viewRenderService.RenderToStringAsync("UserManagement/_EditUserClaims", await GetUserClaims(id))
            });
        }

        [ClaimRequirement("UserManagement", "ManageOtherUsers")]
        public async Task<IActionResult> AddClaims(string id, string claims)
        {
            await _rep.AddClaimsToUser(id, claims, _userManager.GetUserId(User));

            var model = await GetUserClaims(id);

            return Json(new
            {
                success = 1,
                page = await _viewRenderService.RenderToStringAsync("UserManagement/_EditUserClaimsModal", model)
            });
        }

        [ClaimRequirement("UserManagement", "ManageOtherUsers")]
        public async Task<IActionResult> RemoveClaims(string id, string claims)
        {
            await _rep.RemoveClaimsFromUser(id, claims, _userManager.GetUserId(User));

            var model = await GetUserClaims(id);

            return Json(new
            {
                success = 1,
                page = await _viewRenderService.RenderToStringAsync("UserManagement/_EditUserClaimsModal", model)
            });
        }

        [ClaimRequirement("UserManagement", "ManageOtherUsers")]
        public async Task<IActionResult> SetUserLockout(string id, int lockout)
        {
            await _rep.SetUserLockout(id, _userManager.GetUserId(User), lockout == 1);

            var users = (await _rep.GetUsers()).Where(x => x.Id != _userManager.GetUserId(User)).ToList();
            return Json(new
            {
                success = 1,
                page = await _viewRenderService.RenderToStringAsync("UserManagement/_ManageOtherUsers", users)
            });
        }


        [ClaimRequirement("UserManagement", "ManageMyAccount")]
        public async Task<IActionResult> UpdatePersonalDetails(ApplicationUser user)
        {
            // Confirm the correct user is making this change
            if (user.Id != _userManager.GetUserId(User))
                return Json(new
                {
                    success = 0
                });

            var result = await _rep.UpdateUserDetails(user);

            if (result == -1)
                return Json(new
                {
                    success = result,
                    error = "That username is already in user. Please enter a different one"
                });

            return Json(new
            {
                success = result
            });
        }

        [ClaimRequirement("UserManagement", "ManageMyAccount")]
        public async Task<IActionResult> ChangePassword(ApplicationUser user)
        {
            // Confirm the correct user is making this change
            if (user.Id != _userManager.GetUserId(User))
                return Json(new
                {
                    success = 0
                });

            var result = await _rep.UpdateUserPassword(user);
            return Json(new
            {
                success = result
            });
        }

        // Operations
        private async Task<EditUserClaimsWrap> GetUserClaims(string id)
        {
            var claims = new List<EditUserClaims>();
            var user = await _userManager.FindByIdAsync(id);
            var userclaims = await _userManager.GetClaimsAsync(user);
            var allclaims = await _rep.GetClaimsList();

            // selected
            foreach (var claim in userclaims)
            {
                var userclaim = claims.FirstOrDefault(x => x.Controller == claim.Type);

                if (userclaim == null)
                {
                    userclaim = new EditUserClaims
                    {
                        Controller = claim.Type
                    };

                    claims.Add(userclaim);
                }

                userclaim.SelectedClaims.Add(claim.Value);
            }

            // unselected
            foreach (var claim in allclaims)
            {
                var userclaim = claims.FirstOrDefault(x => x.Controller == claim.Controller);
                if (userclaim == null)
                {
                    userclaim = new EditUserClaims
                    {
                        Controller = claim.Controller
                    };

                    claims.Add(userclaim);
                }

                if (userclaim.Controller == claim.Controller && !userclaim.SelectedClaims.Contains(claim.Action))
                    userclaim.UnSelectedClaims.Add(claim.Action);
            }

            return new EditUserClaimsWrap
            {
                UserId = id,
                Claims = claims
            };
        }
    }
}